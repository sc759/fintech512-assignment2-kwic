package KWIC;
import java.util.*;

public class IgnoreDict {
    final private Set<String> words;

    public IgnoreDict() {
        words = new HashSet<>();
    }

    public void addNonKeyword(String str) {
        words.add(str.toLowerCase(Locale.ROOT));
    }

    public boolean isKeyword(String str) {
        for (String w : words) {
            if (str.equals(w)) {
                return false;
            }
        }
        return true;
    }
}
