package KWIC;
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        IgnoreDict id = new IgnoreDict();
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        while (!line.equals("::")) {
            id.addNonKeyword(line);
            line = scanner.nextLine();
        }
        ArrayList<Title> titles = new ArrayList<>();
        line = scanner.nextLine();
        while (!line.equals("") && scanner.hasNextLine()) {
            titles.add(new Title(line, id));
            line = scanner.nextLine();
        }
        for (Title title : titles) {
            for (int i = 0; i < title.SizeKWIC(); i++) {
                System.out.println(title.TitleKWIC(i));
            }
        }
    }
}
