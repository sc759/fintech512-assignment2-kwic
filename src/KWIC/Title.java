package KWIC;

import java.util.ArrayList;
import java.util.Locale;

public class Title {

    private static class Word {
        public String str;
        public int start;

        public Word(String str, int start) {
            this.str = str;
            this.start = start;
        }
    }

    final private ArrayList<Word> keywords;
    final private String title;
    static String DELIMINATOR = ",.'\"?;:(){}[]/!$* ";

    private boolean isDeliminator(char c) {
        return DELIMINATOR.indexOf(c) >= 0;
    }

    private void assembleWord(String str, int start, int pivot, IgnoreDict ignoreDict) {
        boolean is_dim_start = isDeliminator(str.charAt(start));
        if (!is_dim_start) {
            String word = str.substring(start, pivot);
            if (ignoreDict.isKeyword(word.toLowerCase(Locale.ROOT))) {
                keywords.add(new Word(word, start));
            }
        }
    }

    private int parseWord(String str, int start, int pivot, IgnoreDict ignoreDict) {
        boolean is_dim_start = isDeliminator(str.charAt(start));
        boolean is_dim_pivot = isDeliminator(str.charAt(pivot));
        if (is_dim_start == is_dim_pivot) {
            return start;
        }
        assembleWord(str, start, pivot, ignoreDict);
        return pivot;
    }

    public Title(String str, IgnoreDict ignoreDict) {
        title = str;
        keywords = new ArrayList<>();
        if (str.length() == 0) {
            return;
        }
        int start = 0;
        int i;
        for (i = 0; i < str.length(); i++) {
            start = parseWord(str, start, i, ignoreDict);
        }
        assembleWord(str, start, i, ignoreDict);
    }

    public String KeywordAt(int i) {
        if (i >= keywords.size()) {
            return "";
        }
        return keywords.get(i).str;
    }

    public String TitleKWIC(int index) {
        if (index >= keywords.size()) {
            return "";
        }
        Word keyword = keywords.get(index);
        int pivot = keyword.start;
        int end = keyword.start + keyword.str.length();
        return title.substring(0, pivot).toLowerCase(Locale.ROOT)
                + keyword.str.toUpperCase(Locale.ROOT)
                + title.substring(end).toLowerCase(Locale.ROOT);
    }

    public int SizeKWIC() {
        return keywords.size();
    }

}
