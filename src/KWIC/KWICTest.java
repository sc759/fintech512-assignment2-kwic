package KWIC;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;


public class KWICTest {
    @Test
    void IgnoreDictTest1() {
        IgnoreDict id = new IgnoreDict();
        assertTrue(id.isKeyword("xxx"));
    }

    @Test
    void IgnoreDictTest2() {
        IgnoreDict id = new IgnoreDict();
        id.addNonKeyword("keyword");
        assertTrue(id.isKeyword("xxx"));
        assertFalse(id.isKeyword("keyword"));
    }

    @Test
    void IgnoreDictTest3() {
        IgnoreDict id = new IgnoreDict();
        id.addNonKeyword("is");
        id.addNonKeyword("the");
        id.addNonKeyword("of");
        id.addNonKeyword("and");
        id.addNonKeyword("as");
        id.addNonKeyword("a");
        id.addNonKeyword("but");
        assertTrue(id.isKeyword("xxx"));
        assertFalse(id.isKeyword("is"));
        assertFalse(id.isKeyword("the"));
        assertFalse(id.isKeyword("of"));
        assertFalse(id.isKeyword("and"));
        assertFalse(id.isKeyword("as"));
        assertFalse(id.isKeyword("a"));
        assertFalse(id.isKeyword("but"));

    }

    @Test
    void TitleTest1() {
        IgnoreDict id = new IgnoreDict();
        id.addNonKeyword("keyword");
        Title title = new Title("", id);
        assertEquals("", title.KeywordAt(0));
    }

    @Test
    void TitleTest2() {
        IgnoreDict id = new IgnoreDict();
        id.addNonKeyword("keyword");
        Title title = new Title("Descent of Man", id);
        assertEquals("Descent", title.KeywordAt(0));
        assertEquals("of", title.KeywordAt(1));
        assertEquals("Man", title.KeywordAt(2));
        assertEquals("", title.KeywordAt(3));
    }

    @Test
    void TitleTest3() {
        IgnoreDict id = new IgnoreDict();
        id.addNonKeyword("keyword");
        Title title = new Title("Descent: of Man", id);
        assertEquals("Descent", title.KeywordAt(0));
        assertEquals("of", title.KeywordAt(1));
        assertEquals("Man", title.KeywordAt(2));
        assertEquals("", title.KeywordAt(3));
    }

    @Test
    void TitleTest4() {
        IgnoreDict id = new IgnoreDict();
        id.addNonKeyword("of");
        Title title = new Title("Descent: of Man", id);
        assertEquals("Descent", title.KeywordAt(0));
        assertEquals("Man", title.KeywordAt(1));
        assertEquals("", title.KeywordAt(2));
    }
    @Test
    void TitleAtTest1() {
        IgnoreDict id = new IgnoreDict();
        id.addNonKeyword("of");
        Title title = new Title("Descent: of Man", id);
        assertEquals("DESCENT: of man", title.TitleKWIC(0));
        assertEquals("descent: of MAN", title.TitleKWIC(1));
        assertEquals("", title.TitleKWIC(2));
    }

}
